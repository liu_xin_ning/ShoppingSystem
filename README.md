# 拼夕夕_网上商城

#### 介绍
基于SpringBoot+Mybatis+Vue+Eleement开发的网上商城

### 软件架构
软件架构说明
#### -GoodsAdmin    springMVC 后端系统     
#### -WebShop    Vue+Element web网上商城
#### -WebShopAdmin    Vue+Element web商城管理系统

#### 克隆项目
git clone https://gitee.com/mycyy1/ShoppingSystem.git

### 使用说明

#### 启动GoodsAmin，激活接口 --> 启动WebShop和WebShopAdmin 

### 安装教程
#### SpringMVC
使用IDEA，启动项目，进入pom.xml文件，安装对应依赖包

#### Vue
#### 安装依赖
npm install

#### PS：建议不要直接使用 cnpm 安装依赖，会有各种诡异的 bug。可以通过如下操作解决 npm 下载速度慢的问题
npm install --registry=https://registry.npm.taobao.org

#### 启动服务
npm run serve

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
